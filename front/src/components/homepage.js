import React, {useEffect, useState} from 'react'
import {connect, useDispatch, useSelector} from 'react-redux'
import io from 'socket.io-client'
import { setUsername } from '../redux/actions/userActions';

function HomePage(props){
    const url = "http://localhost:5000";
    const socket = io.connect(url);

    // const [name, setName] = useState("")
    // const [error, setError] = useState("")
    // const [success, setSuccess] = useState("")

    // const dispatch = useDispatch() 
    // const myName = useSelector(state=>state.user.username)


    useEffect(()=>{
        socket.emit("connection")
        
    },[])

    
    // socket.on("display name error",(err)=>{
    //     setError(err)
    // })
    // socket.on("username set", (userName)=>{
    //     dispatch(setUsername(userName))
    //     setSuccess("Username Successfully set")
    //     setName("")
    //     console.log(myName)
    // })
    

    return(
        <div className="container">
            <h1 className="mt-3 text-info">HomePage</h1>
            <h2 className="mt-3 text-socndary">Let's play Rock Scissors Paper</h2>
            <div className="container d-flex flex-column align-items-center">
                {/* <h3 className="text-success">{success}</h3>
                <h3 className="text-danger">{error}</h3>
                <input type="text" placeholder="Username" onChange={(e)=>setName(e.target.value)} value={name}/>
                <button className="btn btn-primary w-25 mt-3" onClick={()=>socket.emit('set username', name)}>Set Username</button> */}
                <a className="btn btn-primary w-25 mt-3" href="/createRoom"  >Create Room</a>
                <a className="btn btn-primary w-25 mt-3" href="/joinRoom" >Join Room</a>
            </div>
        </div>
    )

}

export default connect(r=>r)(HomePage)

