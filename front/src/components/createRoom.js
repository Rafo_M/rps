import React, {useState,useEffect} from 'react'
import { useHistory } from "react-router-dom";
import {connect, useDispatch, useSelector} from 'react-redux'
import {setData} from '../redux/actions/roomActions'
import socketIOClient from 'socket.io-client'


function CreateRoom(props) {

    const url = "http://127.0.0.1:5000"
    const socket = socketIOClient(url)

    const dispatch = useDispatch()
    const history = useHistory()


    const room = useSelector(state=>state.room.roomId)
    const playerId = useSelector(state=>state.room.playerId)

    

    useEffect(()=>{
        dispatch(setData("playerId",1))
    
        
    },[]);

    socket.on("room created",(roomNum)=>{
        console.log(roomNum)
    })

    socket.on("display error",(err)=>{
        console.log(err)
        setError(err)
    })

    socket.on("player 1 connected",()=>{
        console.log("player 1 connected")
        
        console.log("Player ID = ",playerId)
        history.push(`/game:${roomId}`)
    })
    
    const [error, setError] = useState("")
    const [roomId,setRoomId] = useState("")
    return (
        <div>
            <h1> Create Room</h1>
            <div className="container d-flex flex-column align-items-center">
                <h5 className="text-danger">{error}</h5>
                <input 
                type="number" 
                className="form-controll m-3" 
                placeholder="New Room" 
                aria-label="Room"
                onChange={(e)=>setRoomId(e.target.value)}
                />
                <button
                className="btn btn-success mt-3"
                href=""
                onClick={()=>{socket.emit('create-room',roomId);dispatch(setData("roomId",roomId))}}
                >Create New Room</button>
            </div>
            <a className="btn btn-secondary mt-3" href="/">Back</a>
        </div>
    )
}

export default connect(r=>r)(CreateRoom)
