import React, {useState, useEffect} from 'react'
import { useHistory } from 'react-router';
import {connect, useDispatch, useSelector} from 'react-redux'
import {setData} from '../redux/actions/roomActions'
import socketIOClient from 'socket.io-client'


function JoinRoom() {



    const history = useHistory()
    const dispatch = useDispatch()
    const url = "http://127.0.0.1:5000";
    const socket = socketIOClient(url);

    const room = useSelector(state=>state.room.roomId)
    const playerId = useSelector(state=>state.room.playerId)



    const [err, setErr] = useState("")
    const [roomId,setRoomId] = useState("")


    
    useEffect(()=>{
        dispatch(setData("playerId",2))

    },[])

    socket.on("room created",(roomNum)=>{
        console.log(roomNum)
    },[])

    socket.on("display error",(err)=>{
        console.log(err)
        setErr(err)
    })

    socket.on("player 2 connected",()=>{
        console.log("player 2 connected")
        console.log("player Id = ",playerId)
        history.push(`/game:${roomId}`)
    })
    return (
        <div className="container">
            <h1>Join Room</h1>
            <div className="container d-flex flex-column align-items-center">
                <h5 className="text-danger">{err}</h5>
                <input 
                type="text" 
                className="mt-3" 
                placeholder="Room" 
                aria-label="Room" 
                onChange={(e)=>setRoomId(e.target.value)}
                />
                <button 
                class="btn btn-success mt-3 "
                onClick={()=>{socket.emit("join-room",roomId);dispatch(setData("roomId",roomId))}}
                >Join Room</button>
            </div>
            <a class="btn btn-secondary mt-3" href="/">Back</a>
        </div>
    )
}

export default connect(r=>r)(JoinRoom)
