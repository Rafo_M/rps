import React, {useState,useEffect} from 'react'
import {connect, useDispatch, useSelector} from 'react-redux'
import socketIOClient from 'socket.io-client'
import {setScore} from '../redux/actions/roomActions'

function Game() {


    const url = "http://127.0.0.1:5000"
    const socket = socketIOClient(url)
    

    const playerId = useSelector(state=>state.room.playerId)
    const room = useSelector(state=>state.room.roomId)
    const oponentScore = useSelector(state=>state.room.oponentScore)
    const myScore = useSelector(state=>state.room.myScore)
    const myName = useSelector(state=>state.user.username)
    
    const [result, setResult] = useState("Result")

    const dispatch = useDispatch()

    useEffect(()=>{
        
        socket.on("draw",(res)=>{
            console.log(res)
            setResult(res)
        })
        socket.on("player 1 wins",({myChoice,oponentChoice})=>{
            if(playerId == 1){
                console.log("I win")
                setResult(myChoice + " - " +oponentChoice + " I Won")
                dispatch(setScore("myScore"))
            }
            else{
                console.log("Oponent wins")
                setResult(myChoice + " - " + oponentChoice + " Oponent Wins")
                dispatch(setScore("oponentScore"))
            }
            
        })
        socket.on("player 2 wins",({myChoice, oponentChoice})=>{
            if(playerId == 2){
                console.log("I win")
                dispatch(setScore("myScore"))
                setResult(myChoice + " - " +oponentChoice + " I Won")
            }
            else{
                console.log("Oponent wins")
                dispatch(setScore("oponentScore"))
                setResult(myChoice + " - " + oponentChoice + " Oponent Wins")
            }
        })
        
    },[])

    // socket.on("draw",(x)=>{
    //     console.log(x)
    // })
    // if(playerId == 1){
    //     socket.on("player 1 wins",(x)=>{
    //         dispatch(setScore("myScore"))
    //     })
    //     socket.on("player 2 wins",(x)=>{
    //         dispatch(setScore("oponentScore"))
    //     })
    // }
    // else{
    //     socket.on("player 2 wins",(x)=>{
    //         dispatch(setScore("myScore"))
    //     })
    //     socket.on("player 1 wins",(x)=>{
    //         dispatch(setScore("oponentScore"))
    //     })
    // }
    

    

    console.log(room)
    return (
        <div className="container">
            <div className="container d-flex flex-row justify-content-around mt-3">
                <h3>Me</h3>
                <h3>Oponent</h3>
            </div>
            <div className="container d-flex flex-row justify-content-around mt-3">
               <button onClick={()=>socket.emit("make-move",{playerId, myChoice:'rock' ,roomId:room})} className="btn btn-primary">Rock</button>
               <button onClick={()=>socket.emit("make-move",{playerId, myChoice:'paper' ,roomId:room})} className="btn btn-primary">Paper</button>
               <button onClick={()=>socket.emit("make-move",{playerId, myChoice:'scissors' ,roomId:room})} className="btn btn-primary">Scissors</button>
            </div>
            <div className="container mt-2">
               <h3>{myScore}:{oponentScore}</h3>
               <h4>{result}</h4>
            </div>
            <button className="btn btn-danger">Exit</button>
        </div>
 )
}

export default Game
