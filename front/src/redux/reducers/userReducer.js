import userState from '../state/userState'

function userReducer(state = userState, action) {
    let temp = {...state}
    if(action.type === "SETUSERNAME"){
        temp.username = action.username
        console.log("User Reducer",temp)

    }
    return temp
}

export default userReducer
