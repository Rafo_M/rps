import roomState from "../state/roomState"

function roomReducer(state = roomState,action) {
    let temp = {...state}
    if(action.type === "SETDATA"){
        temp[action.key] = action.data
    }
    if(action.type === "SETSCORE"){
        temp[action.who] += 1 
    }

    console.log("Room Reducer", temp)

    return temp
}

export default roomReducer
