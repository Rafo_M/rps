import React from 'react'

export function setData (key, data){
    return{
        type:"SETDATA",
        key,
        data
    }
}

export function setScore (who){
    return{
        type:"SETSCORE",
        who
    }
}
