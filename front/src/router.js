import React from 'react'
import {BrowserRouter,Route} from 'react-router-dom'
import HomePage from './components/homepage'
import CreateRoom from './components/createRoom'
import JoinRoom from './components/joinRoom'
import Game from './components/game'

function Router(){
    return(
        <div>
            <BrowserRouter>
                <Route path="/" exact component={HomePage}/>
                <Route path="/createRoom" component={CreateRoom}/>
                <Route path="/joinRoom" component={JoinRoom}/>
                <Route path="/game:id" component={Game} />
            </BrowserRouter>
        </div>
    )
}

export default Router