const express= require('express')
const http = require('http')
const path = require('path')
const socketio = require('socket.io')

const app = express()
const server = http.createServer(app)

const io = require("socket.io")(server, {
    cors: {
      origin: "http://localhost:3000",
      methods: ["GET", "POST"]
    }
});

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


const {userConnected, connectedUsers, moves, makeMove, choices, initializeChoice,setUserName,userNames} = require("./modules/users");
const {createRoom, joinRoom, exitRoom, rooms} = require("./modules/rooms");
console.log(rooms)
io.on('connection', (socket)=>{
    //check if connected
    console.log('user has connected')

    //emit-on

    socket.on('set username',(userName)=>{
        console.log("client has intended to set username - " + userName)

        if(Object.values(userNames).includes(userName)){
            const nameError = "This username already exists" 
            socket.emit("display name error",nameError)
        }
        else{
            socket.emit("display name error","")
            setUserName(socket.client.id,userName)
            socket.emit("username set", userName)
        }

    })
    
    socket.on("create-room",(roomId)=>{
        // Check if the function call has worked
        console.log( "Client has created room N- ",roomId)

        if(roomId in rooms){
            const error = "This room is busy"
            socket.emit("display error", error)
        }
         else{
            socket.emit("display error", "")
            userConnected(socket.client.id);
            createRoom(roomId,socket.client.id);
            socket.emit("room created", roomId);
            socket.emit("player 1 connected");
            socket.join(roomId);
        }
        console.log(rooms)


    })

    socket.on("join-room",(roomId)=>{

        //Check if the function call has worked
        console.log("Client has joined to room N-",roomId)
        if(roomId in rooms === false){
            const error = "this room does not exist";
            //Send the error to Client
            socket.emit("display error",error);
        }
        else{
            //Clear the error, In case it was previously sent to Client and rendered in HTML
            socket.emit("display error","")
            userConnected(socket.client.id);
            //Join the Room
            joinRoom(roomId,socket.client.id);
            //Inform the Client that room was successfully joined
            socket.emit("room joined", roomId);
            //Inform the Client that player 2 is connected
            socket.emit("player 2 connected");
            socket.join(roomId);
            initializeChoice(roomId);
        }

        console.log(rooms)
    })

    socket.emit('id-request',socket.client.id)

    socket.on("make-move",({playerId, myChoice,roomId})=>{
        //console.log("choices",choices)
        makeMove(playerId, myChoice,roomId);
        //console.log(choices)
        

        if(choices[roomId][0] !== "" && choices[roomId][1] !== ""){
            let firstPlayerChoice = choices[roomId][0];
            let secondPlayerChoice = choices[roomId][1];

            if(firstPlayerChoice === secondPlayerChoice){
                //console.log("draw")
                const message = firstPlayerChoice + " and " + secondPlayerChoice + "is draw" 
                //io.to(roomId).emit(draw) was not working ask how it can be fixed
                io.emit("draw",message)
                // console.log(roomId)
            }    
            else if(moves[firstPlayerChoice] === secondPlayerChoice){
                let oponentChoice = ""

                if(playerId === 1){
                    oponentChoice = secondPlayerChoice
                }
                else{
                    oponentChoice = firstPlayerChoice
                }

                io.emit("player 1 wins",{myChoice,oponentChoice})
            }
            else{
                let oponentChoice = ""

                if(playerId === 1){
                    oponentChoice = secondPlayerChoice
                }
                else{
                    oponentChoice = firstPlayerChoice
                }

                io.emit("player 2 wins",{myChoice,oponentChoice})   
         }
        
         console.log(choices[roomId])
         choices[roomId] = ["",""];

       }

        socket.on("disconnect", () => {
            if(connectedUsers[socket.client.id]){
                let player;
                let roomId;
    
                for(let id in rooms){
                    if(rooms[id][0] === socket.client.id || 
                        rooms[id][1] === socket.client.id){
                        if(rooms[id][0] === socket.client.id){
                            player = 1;
                        }else{
                            player = 2;
                        }
    
                        roomId = id;
                        break;
                    }
                }
    
                exitRoom(roomId, player);
    
                if(player === 1){
                    io.to(roomId).emit("player-1-disconnected");
                }else{
                    io.to(roomId).emit("player-2-disconnected");
                }
            }
        })
    })
})

server.listen(5000, () => console.log("server listening port 5000"))