const connectedUsers = {};
const userNames = {}
const choices = {};
const moves = {
    "scissors":"paper",
    "rock":"scissors",
    "paper":"rock"
};

function initializeChoice  (roomId) {
    
    choices[roomId] = ["",""];
    console.log("initialize",choices)
}

function userConnected(userId) {
    connectedUsers[userId] = true;
    console.log(connectedUsers)
}

function setUserName(userId,userName){
    userNames[userId] = userName
    console.log(userNames)
}

function makeMove  (playerId, myChoice,roomId) {
    //console.log("ste",playerId)
    //console.log(roomId)
    //console.log(choices[roomId])
    if(choices[roomId]){
        //console.log("entry",choices)
        choices[roomId][playerId - 1] = myChoice;
    }
}

module.exports = {connectedUsers,choices,moves, initializeChoice, userConnected, makeMove, setUserName,userNames}